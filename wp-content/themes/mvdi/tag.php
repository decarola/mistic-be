<?php get_header(); ?>

<div class="l-wrapper">

  <?php 
  if ( have_posts() ) :

    get_template_part( 'template-parts/content', 'tag' );

  else :

    get_template_part( 'template-parts/content', 'none' );

  endif; 
  ?>

</div><!-- .l-wrapper -->

<?php
get_sidebar();
get_footer();
