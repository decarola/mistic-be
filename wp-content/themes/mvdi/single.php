<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package MVdI
 */

get_header(); ?>

<div class="l-wrapper">

	<?php
	while ( have_posts() ) : the_post();

		get_template_part( 'template-parts/content', get_post_format() );
		?>
		<hr>
		<div class="pagination clearfix">
			<p>Guarda anche:</p>
			<div class="alignleft"><?php previous_post_link(); ?></div>
			<div class="alignright"><?php next_post_link(); ?></div>
		</div>
		<?php

	endwhile; // End of the loop.
	?>

</div><!-- .l-wrapper -->

<?php
get_sidebar();
get_footer();
