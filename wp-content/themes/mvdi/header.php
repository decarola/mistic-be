<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package MVdI
 */

?><!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link href="https://fonts.googleapis.com/css?family=Roboto:100,400,400i,900" rel="stylesheet"> 
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/favicon.png">
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<a class="skip-link screen-reader-text" href="#main">Vai direttamente al contenuto</a>

<?php
$description = get_bloginfo( 'description', 'display' );
if ( $description || is_customize_preview() ) : ?>
	<p class="header__info"><?php echo $description; /* WPCS: xss ok. */ ?></p>
<?php
endif; ?>

<header class="header" role="banner">
  <div class="l-wrapper">
    <a class="header__logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
      <picture>
        <source type="image/svg+xml" srcset="<?php echo get_stylesheet_directory_uri(); ?>/assets/logo/svg/logo.svg">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/logo/logo-esteso.png" alt="Museo Virtuale di Informatica">
      </picture>
    </a>

    <nav id="id-header__nav" class="header__nav nav" role="navigation">
      <!-- <button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'mvdi' ); ?></button> -->
      <?php 
        $menuVar = array(
          'theme_location' => 'primary', 
          'menu_class' => 'nav__pages',
          'depth' => 1
          );
        wp_nav_menu($menuVar); 
        ?>
    </nav><!-- #site-navigation -->

  </div>
</header>

<main class="main page-evidenza curr-<?php get_page_template(); ?>" role="main">