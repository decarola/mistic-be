<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package MVdI
 */

?>

  <?php get_template_part( 'virtual-rooms' ); ?>

</main>

<footer class="footer" role="contentinfo">
  <div class="l-wrapper">

    <nav class="footer__nav nav" role="navigation">
      <?php wp_nav_menu( array( 'menu' => 'Footer menù', 'menu_class' => 'nav__pages nav__pages--primary' ) ); ?>
      <?php wp_nav_menu( array( 'menu' => 'crediti', 'menu_class' => 'nav__pages nav__pages--secondary' ) ); ?>
    </nav>

  </div><!-- .l-wrapper -->
</footer><!-- .footer -->
<?php wp_footer(); ?>
</body>
</html>
