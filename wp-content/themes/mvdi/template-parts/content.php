<section class="clearfix">
  <?php 
  get_template_part("pageFather");

  global $post;
  the_title( '<h1 id="'.$post->post_name.'" class="page-title">', '</h1>' ); ?>

  <?php if ( has_excerpt()) : ?>
    <?php $abstract = get_the_excerpt(); ?>
    <h2><?php echo $abstract; ?></h2>
  <?php endif; ?>

  <?php 
  if ( has_post_thumbnail()) : 
  ?>
  <img class="main-thumb" src="<?php the_post_thumbnail_url(); ?>" alt="museo dell'informatica">
  <?php 
  endif;

  the_content(); 

  // se è una pagina figlia di virtual room - stampa le stanze
  global $post;     // if outside the loop
  $virtualRoom = get_page_by_path('tour-virtuale');
  if  (is_page() && $post->post_parent && ($virtualRoom->ID == $post->post_parent))
    get_template_part("category-tag-filter");

  ?>
</section>