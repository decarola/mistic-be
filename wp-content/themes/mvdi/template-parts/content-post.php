<?php 

$categoryObj = get_the_category()[0]; 
$categorySlug = $categoryObj->slug; 
$categoryName = $categoryObj->name; 

$tag = get_the_tags()[0];
$tagSlug = $tag->slug;
$tagName = $tag->name;

?>

<section class="clearfix">
  <h2 class="main__subtitle">
    <a href="<?php echo 'tag/'.$tagSlug.'/?category='.$categorySlug; ?>"><?php echo $categoryName.",<br>".$tagName;?></a>
  </h2>
	<?php the_title( '<h1 class="page-title">', '</h1>' ); ?>

  <?php if ( has_excerpt()) : ?>
    <?php $abstract = get_the_excerpt(); ?>
    <h2><?php echo $abstract; ?></h2>
  <?php endif; ?>

  <?php if ( has_post_thumbnail()) : ?>
  <img class="main-thumb" src="<?php the_post_thumbnail_url(); ?>" alt="museo dell'informatica">
  <?php endif; ?>

  <?php the_content(); ?>


</section>