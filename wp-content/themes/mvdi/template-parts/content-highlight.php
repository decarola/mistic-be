<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package MVdI
 */

?>

<div class="welcome post-<?php the_ID(); ?>">

  <?php //the_title( '<h1 class="welcome__title">', '</h1>' ); ?>
	<?php if ( has_excerpt()) : ?>
	<?php $abstract = get_the_excerpt(); ?>
	<h1 class="welcome__title"><?php echo $abstract; ?></h1>
	<?php endif; ?>

  <div class="welcome__abstract">
  	<?php the_content(); ?>
  </div>

  <div class="welcome__mainimg">
    <img src="<?php the_post_thumbnail_url(); ?>" alt="museo dell'informatica">
  </div>
</div>

	<footer class="entry-footer">
		<?php mvdi_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
