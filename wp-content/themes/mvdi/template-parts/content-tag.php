<?php 

$tagAndCategory = false;
if ( is_tag() && isset($_GET["category"]) ) {
  $tagAndCategory = true;

  $cat = get_category_by_slug($_GET['category']);
  $tagId = get_query_var('tag_id');
}

if ($tagAndCategory) {
  echo '<h2 class="main__subtitle">'.$cat->name.'</h2>';
}
?>

<h1 class="page-title"><?php single_cat_title(); ?></h1>

<div class="grid-3 no-gutter">
  <?php
    if ($tagAndCategory) {
      global $wp_query;
      $args = array(
        'category__and' => $cat->term_id, 
        'tag__in' =>  $tagId, //must use tag id for this field
        'posts_per_page' => -1); //get all posts
      $posts = get_posts($args);
      
      global $post;
      foreach ($posts as $post) {
        setup_postdata($post);
        get_template_part( 'gallery', 'element' );
      }
    } else {
     /* Start the Loop */
      while ( have_posts() ) : the_post(); 
        get_template_part( 'gallery', 'element' );
      endwhile;
    }
  ?>
</div><!-- .grid-3  -->

<?php if ($tagAndCategory) : ?>
  <hr>
  <p><a href="<?php echo esc_url( home_url( '/' ) ); ?><?php echo $cat->slug; ?>">Torna alla sezione <em><?php echo $cat->name; ?></em></a></p>
<?php else: ?>
<div class="pagination">
  <div class="nav-previous alignleft"><?php next_posts_link( 'Older posts' ); ?></div>
  <div class="nav-next alignright"><?php previous_posts_link( 'Newer posts' ); ?></div>
</div>
<?php endif; ?>