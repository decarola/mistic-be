<?php
  global $post;

  $father = get_post_ancestors( $post );
  if (count($father)>0) {
    $fatherPost = get_post(array_shift($father));
    $perma = get_permalink($fatherPost);
    echo '<h2 class="main__subtitle">'.$fatherPost->post_title.'</h2>';
  }
?>