<?php
/**
 * La lista delle camere virtuali nel museo
 *
 */

?><section class="virtual-rooms">
  <div class="l-wrapper">
    <h2 class="virtual-rooms__title">Le stanze del museo:</h2>
    <ul class="virtual-rooms__list list-feed">
      <?php 
      $tourVirtualePage = get_page_by_path("tour-virtuale");
      $virtualrooms_arr = get_page_children($tourVirtualePage->ID, get_pages(array('parent' => $tourVirtualePage->ID,'sort_column' => 'menu_order')));
      foreach ($virtualrooms_arr as $roomWpObj) {
        echo '<li><a href="'.esc_url( home_url( '/' ) ).$roomWpObj->post_name.'" >'.$roomWpObj->post_title.'</a></li>';
      }
        // $virtualroom_arr = array(
        //   'exclude' => '1',
        //   'hide_title_if_empty' => true,
        //   'hide_empty' => false, 
        //   'title_li' => ''
        //   );
        // wp_list_categories($virtualroom_arr); 
        ?>
    </ul>
  </div>
</section>