<div class="col">
  <a class="gallery" href="<?php the_permalink(); ?>" data-permalink="<?php the_permalink(); ?>">
  <div class="gallery__thumb">
  <?php if ( has_post_thumbnail()) : ?>
    <img class="gallery__img" src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>">
  <?php else: ?>
    <img class="gallery__img" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/logo/logo-medio.png" alt="<?php the_title(); ?>">
  <?php endif; ?>
  </div>
  <p class="gallery__title"><?php the_title(); ?></p>
  </a>
</div>
