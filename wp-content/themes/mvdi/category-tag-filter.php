<?php
/**
 * List all tags relativa to a single category
 */
?>
<p>Filtra in base alle tipologie di materiale disponibile:</p>
<ul class="list-tags">
  <?php
  foreach (get_tags() as $wpObj) {
    $resArr[$wpObj->name] = "tag/".$wpObj->slug."/?category=".$post->post_name;
  }
  foreach ($resArr as $key => $value) {
    echo '<li><a href="'.esc_url( home_url( '/' ) ).$value.'">'.$key.'</a></li>';
  }

   // Cronologia
  $children_array = get_children( array('post_parent' => get_the_ID()) ); 
  if (count($children_array) > 0) {
    $childrenPost = get_post(array_pop($children_array)->ID);
    $perma = get_permalink($childrenPost);
    echo '<li><a href="'.$perma.'">'.$childrenPost->post_title.'</a></li>';

  }



  ?>
</ul>
